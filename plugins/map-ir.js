import Vue from 'vue'
import {
  mapir, mapNavigationControl, mapGeolocateControl, mapPopup, mapMarker, mapGeojsonLayer, mapFullscreenControl
} from 'mapir-vue'

// eslint-disable-next-line vue/multi-word-component-names
Vue.component('Mapir', mapir)
Vue.component('MapNavigationControl', mapNavigationControl)
Vue.component('MapGeolocateControl', mapGeolocateControl)
Vue.component('MapPopup', mapPopup)
Vue.component('MapMarker', mapMarker)
Vue.component('MapGeojsonLayer', mapGeojsonLayer)
Vue.component('MapFullscreenControl', mapFullscreenControl)
