/* eslint-disable no-undef */
import Vue from 'vue'

export default function ({ $axios, store }) {
  if (process.client) {
    $axios.defaults.baseURL = process.env.APP_SERVICE_URL
  }

  $axios.onRequest((config) => {
    // if (!navigator.onLine) { console.log('Network Connection Error') }
  })

  $axios.onError((error) => {
    // The request was made and the server responded with a status code
    const code = parseInt(error.response && error.response.status)
    switch (code) {
      case 401:
        console.log('Your Token Expired')
        // store.dispatch('logoutLocally')
        return false
      case 403:
        console.log('Forbidden Access')
        Vue.prototype.$router.go(-1)
        return false
      case 500:
        console.log('Server Error')
        return false
      case 422:
        console.log('422 Error')
        return false
    }

    // const codeResponse = parseInt(error.response.data.status)
    // switch (codeResponse) {
    //   case 400001:
    //     Vue.prototype.$toasted.show('نام کاربری یا رمز عبور نامعتبر است')
    //     return false
    //   case 400002:
    //     Vue.prototype.$toasted.show('نام کاربری نامعتبر است')
    //     return false
    // }
  })
}
