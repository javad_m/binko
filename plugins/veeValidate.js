/* eslint-disable camelcase */
import Vue from 'vue'
import { extend, ValidationProvider, ValidationObserver } from 'vee-validate'
import { required, required_if, confirmed, alpha, alpha_spaces, numeric, length, min, max, max_value, email, regex } from 'vee-validate/dist/rules'

extend('required', {
  ...required,
  message: 'این قسمت الزامی است'
})

extend('required_if', {
  ...required_if,
  message: 'این قسمت الزامی است'
})

extend('confirmed', {
  ...confirmed,
  message: 'این فیلد معتبر نمیباشد'
})

extend('check_number', {
  ...required_if,
  message: 'شماره تلفن صحبح را وارد کنید'
})

extend('alpha_spaces', {
  ...alpha_spaces,
  message: 'فقط از حروف میتوانید استفاده کنید'
})

extend('alpha', {
  ...alpha,
  message: 'این قسمت فقط باید دارای حروف الفبا باشد'
})

extend('numeric', {
  ...numeric,
  message: 'این فیلد از اعداد تشکیل میشود'
})

extend('length', {
  ...length,
  message: 'اطلاعات وارد شده این فیلد معتبر نمیباشد'
})

extend('min', {
  ...min,
  message: 'اطلاعات وارد شده این فیلد معتبر نمیباشد'
})

extend('max', {
  ...max,
  message: 'اطلاعات وارد شده این فیلد معتبر نمیباشد'
})

extend('max_value', {
  ...max_value,
  message: 'اطلاعات وارد شده این فیلد معتبر نمیباشد'
})

extend('email', {
  ...email,
  message: 'پست الکترونیکی را صحیح بنویسید'
})

extend('regex', {
  ...regex,
  message: 'اطلاعات وارد شده این فیلد معتبر نمیباشد'
})

extend('profit', {
  validate: (value, { decimals = '*', separator = '.' } = {}) => {
    if (value === null || value === undefined || value === '') {
      return {
        valid: false
      }
    }
    if (Number(decimals) === 0) {
      return {
        valid: /^-?\d*$/.test(value)
      }
    }
    const regexPart = decimals === '*' ? '+' : `{1,${decimals}}`
    const regex = new RegExp(`^[-+]?\\d*(\\${separator}\\d${regexPart})?([eE]{1}[-]?\\d+)?$`)
    return {
      valid: regex.test(value),
      data: {
        serverMessage: 'Only decimal values are available'
      }
    }
  },
  message: 'مقدار وارد شده صحیح نمی باشد'
})
Vue.component('ValidationObserver', ValidationObserver)
Vue.component('ValidationProvider', ValidationProvider)
