export default class LocalScheme {
  constructor (auth, options) {
    this.$auth = auth
    this.name = options._name

    this.options = Object.assign({}, DEFAULTS, options)
  }

  _setToken (token) {
    if (this.options.globalToken) {
      // Set Authorization token for all axios requests
      this.$auth.ctx.app.$axios.setHeader(this.options.tokenName, token)
    }
  }

  _setData (data) {
    // this.$cookies.set('client-token', response.data.data.token, {
    //   maxAge: 60 * 60 * 24 * 30
    // })
    // this.$axios.setHeader('authorization', 'Bearer ' + response.data.data.token)
    // this.$store.commit("user/setInfo", response.data.data)
    // this.$store.commit("user/setUserCanCreateOrg", this.checkUserRole(response.data.data.organization, response.data.data.user_id))
    // let user = {
    //   user_id: response.data.data.user_id,
    //   first_name: response.data.data.first_name,
    //   last_name: response.data.data.last_name,
    //   mobile: response.data.data.mobile,
    //   canCreateOrg: this.checkUserRole(response.data.data.organization, response.data.data.user_id)
    // }
    // this.$cookies.set("user_info", user, {
    //   maxAge: 60 * 60 * 24 * 30
    // })
    // this.$store.commit("user/login");
    // this.$router.push({ path: '/profile/menu' })
  }

  _clearToken () {
    if (this.options.globalToken) {
      // Clear Authorization token for all axios requests
      this.$auth.ctx.app.$axios.setHeader(this.options.tokenName, false)
    }
  }

  mounted () {
    if (this.options.tokenRequired) {
      const token = this.$auth.syncToken(this.name)
      this._setToken(token)
    }

    return this.$auth.fetchUserOnce()
  }

  async login (endpoint) {
    if (!this.options.endpoints.login) {
      return
    }

    // Ditch any leftover local tokens before attempting to log in
    await this._logoutLocally()

    const result = await this.$auth.request(endpoint, this.options.endpoints.login)
    // set store
    localStorage.setItem('user', JSON.stringify(result))

    // await this._setScope(result)

    if (this.options.tokenRequired) {
      const token = this.options.tokenType ? this.options.tokenType + ' ' + result.token : result.token
      this.$auth.setToken(this.name, token)
      this._setToken(token)
    }

    return this.fetchUser()
  }

  async setUserToken (tokenValue) {
    // Ditch any leftover local tokens before attempting to log in
    await this._logoutLocally()

    if (this.options.tokenRequired) {
      const token = this.options.tokenType
        ? this.options.tokenType + ' ' + tokenValue
        : tokenValue

      this.$auth.setToken(this.name, token)
      this._setToken(token)
    }

    return this.fetchUser()
  }

  fetchUser (endpoint) {
    // User endpoint is disabled.
    if (!this.options.endpoints.user) {
      this.$auth.setUser({})
      return
    }

    // Token is required but not available
    if (this.options.tokenRequired && !this.$auth.getToken(this.name)) {
      return
    }

    // Try to fetch user and then set
    /* const user = await this.$auth.requestWith(
          this.name,
          endpoint,
          this.options.endpoints.user
      ) */
    const user = JSON.parse(localStorage.getItem('user')) || {}

    this.$auth.setUser(user)
  }

  async logout (endpoint) {
    // Only connect to logout endpoint if it's configured
    if (this.options.endpoints.logout) {
      await this.$auth
        .requestWith(this.name, endpoint, this.options.endpoints.logout)
        .catch(() => {
          console.log('error in logout')
        })
    }
    // localStorage.clear()

    // But logout locally regardless
    return this._logoutLocally()
  }

  _logoutLocally () {
    if (this.options.tokenRequired) {
      this._clearToken()
    }

    return this.$auth.reset()
  }
}

const DEFAULTS = {
  tokenRequired: true,
  tokenType: 'Bearer',
  globalToken: true,
  tokenName: 'Authorization'
}
