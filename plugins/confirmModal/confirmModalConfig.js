import ConfirmModal from './ConfirmModal'
import { events } from './events.js'

export default {
  install (Vue, args = {}) {
    if (this.installed) { return }

    this.installed = true
    this.params = args

    Vue.component(args.componentName || 'ConfirmModal', ConfirmModal)

    const confirm = (params) => {
      if (typeof params === 'object') {
        events.$emit('open', params)
      }
    }
    confirm.close = () => {
      events.$emit('close')
    }

    Vue.prototype.$confirmModal = confirm
    Vue.$confirmModal = confirm
  }
}
