import Vue from 'vue'
import confirmModal from '~/plugins/confirmModal/confirmModalConfig'

Vue.use(confirmModal)
