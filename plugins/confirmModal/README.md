Quick Start Usage

for Nuxt.js
add index in plugins nuxt.config.js

add To Template
<ConfirmModal />

In methods :

methods: {
    submit(){
      this.$confirmModal(
        {
          title: 'title',
          message: 'message',
          button: {
            no: 'No',
            yes: 'Yes'
          },
          callback: confirm => {
            if (confirm) {
            }
          }
        }
      )
    }
  }