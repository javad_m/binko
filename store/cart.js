export const state = () => ({
  addressSelected: null,
  deliverySelected: null,
  deliveryTypes: [],
  products: [],
  invoice: null,
  description: ''
})
export const mutations = {
  setAddressSelected (state, value) {
    state.addressSelected = value
  },
  setDeliverySelected (state, value) {
    state.deliverySelected = value
  },
  updateDeliveries (state, items) {
    state.deliveryTypes = items
  },
  updateDescription (state, value) {
    state.description = value
  },
  setInvoiceInfo (state, value) {
    state.invoice = value
  },
  clear (state) {
    state.products = []
    state.addressSelected = null
    state.deliverySelected = null
    state.invoice = null
    state.description = ''
  },
  remove (state, product) {
    state.products.forEach((item, index) => {
      if (item.product_id === product.product_id) { state.products.splice(index, 1) }
    })
  },
  minus (state, product) {
    state.products.forEach((item, index) => {
      if (item.product_id === product.product_id) { item.quantity-- }
    })
  },
  plus (state, product) {
    state.products.forEach((item, index) => {
      if (item.product_id === product.product_id) { item.quantity++ }
    })
  },
  add (state, product) {
    state.products.push(product)
  }
}
