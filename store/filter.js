
export const state = () => ({
  filterSelect: null,
  categorySelect: 'all',
  search: '',
  filteredText: '',
  categories: [],
  filters: [
    {
      id: 1,
      title: 'ارزانترین ها'
    },
    {
      id: 2,
      title: 'گرانترین ها'
    },
    {
      id: 3,
      title: 'پرتخفیف ترین ها'
    },
    {
      id: 4,
      title: 'پرفروش ترین ها'
    },
    {
      id: 5,
      title: 'جدیدترین ها'
    },
    {
      id: 6,
      title: 'نتایج جستجو'
    }
  ],
  selectedFilter: 'all'
})
export const mutations = {
  updateFilterSelect (state, key) {
    state.filterSelect = key
  },
  updateSelectedFilter (state, key) {
    state.selectedFilter = key
  },
  updateCategorySelect (state, key) {
    state.categorySelect = key
  },
  updateSearch (state, value) {
    state.search = value
  },
  updateFilteredText (state, value) {
    state.filteredText = value
  },
  updateCategories (state, items) {
    state.categories = items
  },
  updateFilters (state, items) {
    state.filters = items
  },
  reset (state) {
    state.filterSelect = null
    state.categorySelect = 'all'
    state.search = ''
    state.filterSelect = ''
    state.filteredText = ''
  }
}
