
export const state = () => ({
  activeSplashScreen: true,
  title: 'بینکو',
  pwa_popup_show: false,
  app_version: '0.0.1'
})
export const mutations = {
  changeLoadingStatus (state, status) {
    state.activeSplashScreen = status
  },
  pwaPopupState (state, status) {
    state.pwa_popup_show = status
  }
}
