export const strict = false

export const state = () => ({
  roles: []
})
export const actions = {
  logout ({ commit }) {
    return new Promise((resolve, reject) => {
      this.$auth.logout()
      commit('LOG_OUT')
    })
  },
  logoutLocally ({ commit }) {
    return new Promise((resolve, reject) => {
      this.$auth.reset()
      commit('LOG_OUT')
    })
  }
}
export const mutations = {
  LOG_OUT (state) {
    state.auth.loggedIn = false
    state.auth.user = {}
    this.$router.push('/login')
  }
}
