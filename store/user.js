export const state = () => ({
  address_list: [],
  info: null
})
export const mutations = {
  updateAddressList (state, items) {
    state.address_list = items
  },
  updateUserInfo (state, item) {
    state.info = item
  }
}
