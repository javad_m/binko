export const state = () => ({
  vipProduct: [],
  categories: []
})
export const mutations = {
  updateVipProduct (state, items) {
    state.vipProduct = items
  },
  updateCategories (state, items) {
    state.categories = items
  }
}
