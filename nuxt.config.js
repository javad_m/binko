import path from 'path'
import fs from 'fs'

export default {
  telemetry: false,
  // Global page headers: https://go.nuxtjs.dev/config-head
  head: {
    titleTemplate: '%s - بینکو',
    title: 'بینکو',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=0' },
      { hid: 'description', name: 'description', content: 'بینکو, بزرگترین بازار آنلاین مازندران' },
      { name: 'format-detection', content: 'telephone=no' },
      { name: 'apple-mobile-web-app-capable', content: 'yes' },
      { name: 'apple-mobile-web-app-status-bar-style', content: '#D81726' },
      { name: 'msapplication-TileColor', content: '#D81726' },
      { name: 'theme-color', content: '#D81726' }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }
    ]
  },

  // Global CSS: https://go.nuxtjs.dev/config-css
  css: [
    '@/assets/css/main.css',
    // '@/assets/css/form.scss',
    '@/assets/css/persianfonts.css'
  ],

  // Plugins to run before rendering page: https://go.nuxtjs.dev/config-plugins
  plugins: [
    { src: '@/plugins/axios.js' },
    { src: '@/plugins/map-ir.js', mode: 'client' },
    { src: '@/plugins/cropper.js', mode: 'client' },
    { src: '@/plugins/countdown.js', mode: 'client' },
    { src: '@/plugins/vue-scroll-loader.js', mode: 'client' },
    { src: '@/plugins/confirmModal/index.js', mode: 'client' },
    { src: '@/plugins/veeValidate.js', mode: 'client' }
  ],

  // Auto import components: https://go.nuxtjs.dev/config-components
  components: true,

  // Modules for dev and build (recommended): https://go.nuxtjs.dev/config-modules
  buildModules: [
    // https://go.nuxtjs.dev/eslint
    '@nuxtjs/eslint-module',
    // https://go.nuxtjs.dev/vuetify
    ['@nuxtjs/vuetify', { theme: { dark: false }, rtl: true }],
    ['@nuxtjs/dotenv', { filename: `.env.${process.env.ENV}` }],
    '@nuxtjs/tailwindcss',
    'cookie-universal-nuxt',
    'nuxt-moment-jalaali',
    '@nuxtjs/device'
  ],

  // Modules: https://go.nuxtjs.dev/config-modules
  modules: [
    // https://go.nuxtjs.dev/axios
    '@nuxtjs/axios',
    // https://go.nuxtjs.dev/pwa
    '@nuxtjs/pwa',
    // https://go.nuxtjs.dev/content
    '@nuxt/content',
    '@nuxtjs/dotenv',
    '@nuxtjs/auth',
    ['vue-toastification/nuxt', {
      timeout: 4000,
      draggable: false,
      rtl: true,
      transition: 'Vue-Toastification__slideBlurred'
    }]
  ],

  auth: {
    strategies: {
      local: {
        endpoints: {
          login: { url: '/login', method: 'post', propertyName: 'data.token' },
          logout: false,
          user: { url: '/user/show', method: 'get', propertyName: 'data' }
        },
        tokenRequired: true,
        tokenType: 'Bearer'
      }
    },
    redirect: {
      login: '/login',
      logout: '/',
      user: '/profile',
      callback: '/'
    }
  },
  // Axios module configuration: https://go.nuxtjs.dev/config-axios
  axios: {
    // Workaround to avoid enforcing hard-coded localhost:3000: https://github.com/nuxt-community/axios-module/issues/308
    baseURL: process.env.APP_SERVICE_URL
  },

  loading: {
    name: 'chasing-dots',
    // color: '#ff5638',
    // background: 'white',
    height: '4px'
  },
  // PWA module configuration: https://go.nuxtjs.dev/pwa
  pwa: {
    manifest: {
      lang: 'fa',
      name: 'بینکو',
      short_name: 'بینکو',
      background_color: '#fff',
      theme_color: '#D81726',
      start_url: '/splash'
    },
    meta: {
      theme_color: '#D81726',
      mobileAppIOS: true,
      appleStatusBarStyle: '#D81726',
      lang: 'en'
    },
    icon: {
      source: 'static/icon.png',
      filename: 'icon.png'
    },
    workbox: {
      // dev: true
    }
  },

  // Content module configuration: https://go.nuxtjs.dev/config-content
  content: {},

  // Vuetify module configuration: https://go.nuxtjs.dev/config-vuetify
  vuetify: {
    // customVariables: ['~/assets/variables.scss'],
    theme: {
      dark: false
      // themes: {
      //   dark: {
      //     primary: colors.blue.darken2,
      //     accent: colors.grey.darken3,
      //     secondary: colors.amber.darken3,
      //     info: colors.teal.lighten1,
      //     warning: colors.amber.base,
      //     error: colors.deepOrange.accent4,
      //     success: colors.green.accent3
      //   }
      // }
    }
  },

  // Build Configuration: https://go.nuxtjs.dev/config-build
  build: {},

  server: {
    https: {
      key: fs.readFileSync(path.resolve(__dirname, 'server.key')),
      cert: fs.readFileSync(path.resolve(__dirname, 'server.crt'))
    },
    port: 8585, // default: 3000
    host: '0.0.0.0' // default: localhost,
  }
}
